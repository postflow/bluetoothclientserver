package ru.slvz_rk.testconnectoverbt

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.root_activity.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.root_activity)

        server.setOnClickListener {
            startActivity(Intent(this, MainActivityServer::class.java))
        }

        client.setOnClickListener {
            startActivity(Intent(this, MainActivityClient::class.java))
        }

        insecureClient.setOnClickListener {
            startActivity(Intent(this, MainActivityInsecureClient::class.java))
        }
    }
}
